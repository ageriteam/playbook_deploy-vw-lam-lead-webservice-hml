#!/bin/bash

DATE=`date +%d/%m/%Y" "%H:%M:%S` 

/usr/local/bin/php /var/www/html/artisan schedule:run 2>&1 | tee -a /var/log/schedulerun-output.log
echo "${DATE} - schedule:run executado." >> /var/log/schedulerun.log
exit 0